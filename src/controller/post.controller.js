const knex = require('../model/db');

const userdata = {

    async createPost(req, res) {

        const postdata = [
            {
                TITLE: req.body.title,
                DESCRIPTION: req.body.description,
                CREATE_AT: req.body.create_at,
                UPDATE_AT: req.body.update_at
            }
        ]
        const created = await knex('postdata').select('ID', 'TITLE', 'DESCRIPTION', 'CREATE_AT', 'UPDATE_AT').insert(postdata);
        res.status(201).json({
            success: true,
            status: 201,
        })
    },
    async showPost(req, res) {
        const getdata = await knex('postdata').select('*');
        res.json({
            success: true,
            getdata
        })
    },
    async updatePost(req, res) {
        const updateid = req.params.updateid;

        const title = req.body.title;
        const description = req.body.description;
        const create_at = req.body.create_at;
        const update_at = req.body.update_at;

        // check update id is present or not
        const checkid = await knex('postdata').select('ID').where({ ID: updateid });
        if (checkid.length != 0) {
            const updatedata = await knex('postdata').update({ TITLE: title, DESCRIPTION: description, CREATE_AT: create_at, UPDATE_AT: update_at }).where({ ID: updateid });
            return res.json({
                success: true,
                message: 'Successfully updated'
            })
        }
        res.json({
            success: false,
            message: "Id doesnot Exist"
        })
    },
    async deletePost(req, res) {
        const checkid = await knex('postdata').select('ID').where({ ID: req.params.deleteid });
        if (checkid.length > 0) {
            const deletedpost = await knex('postdata').delete().where({ ID: req.params.deleteid });
            return res.json({ success: true, message: "post deleted" });
        }
        res.json({ success: false, message: "Id not found" })
    }
}
module.exports = userdata
