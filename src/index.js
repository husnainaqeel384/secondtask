const express = require('express');

const app = express();
const bodyParser = require('body-parser');

const dotenv = require('dotenv')
dotenv.config({ path: '../.env' });

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const user = require('./router/post.routers');
app.use("/v1", user);

app.listen(process.env.PORT, () => {
    console.log(`Server is working on ${process.env.PORT}`);
})
