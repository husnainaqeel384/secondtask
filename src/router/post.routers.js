const express = require('express');
const router = express.Router();

const postcontroller = require('../controller/post.controller')

// create post router
router.route("/createPost").get(postcontroller.createPost);

// Get post router
router.route("/showPost").get(postcontroller.showPost);

//  update post router
router.route("/updatePost/:updateid").put(postcontroller.updatePost);
// delete post router
router.route("/deletePost/:deleteid").delete(postcontroller.deletePost);

module.exports = router;